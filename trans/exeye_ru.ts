<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/qml/ConfigWindow.qml" line="33"/>
        <source>Durations</source>
        <translation>Интервалы</translation>
    </message>
    <message>
        <location filename="../src/qml/ConfigWindow.qml" line="33"/>
        <source>Long break</source>
        <translation>Длинный перерыв</translation>
    </message>
    <message>
        <location filename="../src/qml/ConfigWindow.qml" line="33"/>
        <source>Other options</source>
        <translation>Другие настройки</translation>
    </message>
    <message>
        <location filename="../src/qml/ConfigWindow.qml" line="34"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../src/qml/ConfigWindow.qml" line="34"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
</context>
<context>
    <name>DurationsPage</name>
    <message>
        <location filename="../src/qml/DurationsPage.qml" line="22"/>
        <source>Working time</source>
        <translation>Рабочее время, мин</translation>
    </message>
    <message>
        <location filename="../src/qml/DurationsPage.qml" line="32"/>
        <source>Long break duration</source>
        <translation>Длинный перерыв, мин</translation>
    </message>
    <message>
        <location filename="../src/qml/DurationsPage.qml" line="42"/>
        <source>Short breaks frequency</source>
        <translation>Частота коротких перерывов, мин</translation>
    </message>
    <message>
        <location filename="../src/qml/DurationsPage.qml" line="52"/>
        <source>Postpone time</source>
        <translation>Отсрочка перерыва, мин</translation>
    </message>
</context>
<context>
    <name>LongBreakGallery</name>
    <message>
        <location filename="../src/qml/LongBreakGallery.qml" line="40"/>
        <source>Choose images</source>
        <translation>Выберите изображения</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakGallery.qml" line="43"/>
        <source>Image files</source>
        <translation>Изображения</translation>
    </message>
</context>
<context>
    <name>LongBreakPage</name>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="54"/>
        <source>Old password</source>
        <translation>Старый пароль</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="63"/>
        <source>New password</source>
        <translation>Новый пороль</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="70"/>
        <source>Confirm password</source>
        <translation>Новый пароль еще раз</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="82"/>
        <source>Change</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="86"/>
        <source>Old password is incorrect</source>
        <translation>Неправильный старый пароль</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="92"/>
        <source>New password can not be empty</source>
        <translation>Новый пароль не может быть пустым</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="98"/>
        <source>New passwords do not match</source>
        <translation>Новые пароли не совпадают</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="111"/>
        <location filename="../src/qml/LongBreakPage.qml" line="174"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="143"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="157"/>
        <source>Enter</source>
        <translation>Ввести</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="160"/>
        <source>Password is incorrect</source>
        <translation>Неверный пароль</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="193"/>
        <source>Skip long breaks</source>
        <translation>Пропускать длинные перерывы</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="202"/>
        <source>Allow</source>
        <translation>Разрешить</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="209"/>
        <source>Forbid</source>
        <translation>Запретить</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="216"/>
        <source>Allow with password</source>
        <translation>Разрешить с паролем</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="224"/>
        <source>Block options with password</source>
        <translation>Заблокировать опции паролем</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="224"/>
        <source> (password is not setted)</source>
        <translation> (пароль не задан)</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="235"/>
        <source>Change password</source>
        <translation>Изменить пароль</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="243"/>
        <source>Sound volume</source>
        <translation>Громкость звука</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="255"/>
        <source>Use background wallpapers</source>
        <translation>Использовать фоновые изображения</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakPage.qml" line="257"/>
        <source>Background wallpapers</source>
        <translation>Фоновые изображения</translation>
    </message>
</context>
<context>
    <name>LongBreakWindow</name>
    <message>
        <location filename="../src/qml/LongBreakWindow.qml" line="99"/>
        <source>Long break</source>
        <translation>Длинный перерыв</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakWindow.qml" line="100"/>
        <source>Take a rest</source>
        <translation>Отдохните от компьютера</translation>
    </message>
    <message>
        <location filename="../src/qml/LongBreakWindow.qml" line="117"/>
        <source>Skip</source>
        <translation>Пропустить</translation>
    </message>
</context>
<context>
    <name>OtherOptionsPage</name>
    <message>
        <location filename="../src/qml/OtherOptionsPage.qml" line="25"/>
        <source>Show short break window full screen</source>
        <translation>Показывать окно короткого перерыва на весь экран</translation>
    </message>
    <message>
        <location filename="../src/qml/OtherOptionsPage.qml" line="30"/>
        <source>Remind every 10 minutes when Exeye is stopped</source>
        <translation>Напоминать каждые 10 минут, когда Exeye остановлена</translation>
    </message>
    <message>
        <location filename="../src/qml/OtherOptionsPage.qml" line="35"/>
        <source>Enable activity monitoring</source>
        <translation>Проверка активности за компьютером</translation>
    </message>
    <message>
        <location filename="../src/qml/OtherOptionsPage.qml" line="40"/>
        <source>Enable dark theme</source>
        <translation>Использовать тёмную тему</translation>
    </message>
</context>
<context>
    <name>PostponeWindow</name>
    <message>
        <location filename="../src/qml/PostponeWindow.qml" line="71"/>
        <source>Long break starts at: </source>
        <translation>Перерыв начнется через: </translation>
    </message>
    <message>
        <location filename="../src/qml/PostponeWindow.qml" line="80"/>
        <source>Postpone</source>
        <translation>Отложить</translation>
    </message>
</context>
<context>
    <name>ShortBreakWindow</name>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="66"/>
        <source>Short break</source>
        <translation>Короткий перерыв</translation>
    </message>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="94"/>
        <source>Move your eyes left and right</source>
        <translation>Подвигайте глазами в стороны</translation>
    </message>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="98"/>
        <source>Move your eyes up and down</source>
        <translation>Подвигайте глазами вверх-вниз</translation>
    </message>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="102"/>
        <source>Rotate your eyes</source>
        <translation>Повращайте глазами</translation>
    </message>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="106"/>
        <source>Look at the tip of your nose and forward</source>
        <translation>Переводите глаза с кончика носа вдаль</translation>
    </message>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="110"/>
        <source>Open your eyes wide and close with force</source>
        <translation>Широко откройте глаза и сильно зажмурьте</translation>
    </message>
    <message>
        <location filename="../src/qml/ShortBreakWindow.qml" line="114"/>
        <source>Move your eyes diagonally</source>
        <translation>Подвигайте глазами по диагонали</translation>
    </message>
</context>
<context>
    <name>SpentTimeChart</name>
    <message>
        <location filename="../src/qml/SpentTimeChart.qml" line="40"/>
        <location filename="../src/qml/SpentTimeChart.qml" line="47"/>
        <location filename="../src/qml/SpentTimeChart.qml" line="77"/>
        <source>Inactive</source>
        <translation>Бездействие</translation>
    </message>
    <message>
        <location filename="../src/qml/SpentTimeChart.qml" line="43"/>
        <location filename="../src/qml/SpentTimeChart.qml" line="75"/>
        <source>Work</source>
        <translation>Работа</translation>
    </message>
    <message>
        <location filename="../src/qml/SpentTimeChart.qml" line="45"/>
        <location filename="../src/qml/SpentTimeChart.qml" line="76"/>
        <source>Break</source>
        <translation>Перерыв</translation>
    </message>
    <message>
        <location filename="../src/qml/SpentTimeChart.qml" line="46"/>
        <location filename="../src/qml/SpentTimeChart.qml" line="78"/>
        <source>Stopped</source>
        <translation>Остановлена</translation>
    </message>
</context>
<context>
    <name>StatesLogics</name>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="204"/>
        <source>You was inactive for a long time.</source>
        <translation>Долгое время вы были неактивны.</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="239"/>
        <source>You&apos;ve returned after a long break.</source>
        <translation>Вы вернулись после долгого отсутсвия.</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="247"/>
        <source>Exeye is stopped.</source>
        <translation>Exeye остановлена.</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="258"/>
        <source>Stopped</source>
        <translation>Остановлена</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="260"/>
        <source>Postpone</source>
        <translation>Окончание рабочего времени</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="263"/>
        <source>working time left</source>
        <translation>осталось рабочего времени</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="266"/>
        <source>before short break</source>
        <translation>до короткого перерыва</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="278"/>
        <source>Take a long break</source>
        <translation>Сделать длинный перерыв</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="283"/>
        <source>Start</source>
        <translation>Запустить</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="288"/>
        <source>Stop for ...</source>
        <translation>Остановить на ...</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="291"/>
        <source>30 minutes</source>
        <translation>30 минут</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="295"/>
        <source>1 hour</source>
        <translation>1 час</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="302"/>
        <source>Quit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../src/qml/StatesLogics.qml" line="308"/>
        <source>Exeye was started.</source>
        <translation>Exeye запущен.</translation>
    </message>
</context>
<context>
    <name>StatisticsPage</name>
    <message>
        <location filename="../src/qml/StatisticsPage.qml" line="40"/>
        <source>General info</source>
        <translation>Общее</translation>
    </message>
    <message>
        <location filename="../src/qml/StatisticsPage.qml" line="46"/>
        <source>Start time</source>
        <translation>Время запуска</translation>
    </message>
    <message>
        <location filename="../src/qml/StatisticsPage.qml" line="49"/>
        <source>Total elapsed time</source>
        <translation>Всего прошло время</translation>
    </message>
    <message>
        <location filename="../src/qml/StatisticsPage.qml" line="61"/>
        <source>Number of long breaks</source>
        <translation>Кол-во длинных перерывов</translation>
    </message>
    <message>
        <location filename="../src/qml/StatisticsPage.qml" line="64"/>
        <source>Number of short breaks</source>
        <translation>Кол-во коротких перерывов</translation>
    </message>
    <message>
        <location filename="../src/qml/StatisticsPage.qml" line="72"/>
        <source>Spent time</source>
        <translation>Проведенное время</translation>
    </message>
</context>
</TS>
