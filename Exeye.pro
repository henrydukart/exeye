QT += quick widgets
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += src/main.cpp \
    src/activitymonitor.cpp \
    src/filelogger.cpp \
    src/utils.cpp

RESOURCES += qml.qrc

TRANSLATIONS = trans/exeye_ru.ts

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    qtquickcontrols2.conf \
    src/qml/AboutPage.qml \
    src/qml/CircleProgressBar.qml \
    src/qml/ConfigWindow.qml \
    src/qml/DurationsPage.qml \
    src/qml/ImageSplashScreen.qml \
    src/qml/LongBreakGallery.qml \
    src/qml/LongBreakPage.qml \
    src/qml/LongBreakWindow.qml \
    src/qml/OtherOptionsPage.qml \
    src/qml/PauseTimer.qml \
    src/qml/PostponeWindow.qml \
    src/qml/ShortBreakWindow.qml \
    src/qml/SpentTimeChart.qml \
    src/qml/StatesLogics.qml \
    src/qml/StatisticsPage.qml \
    src/qml/imageSplashScreenFunctions.js \
    trans/exeye_ru.ts

HEADERS += \
    src/activitymonitor.h \
    src/filelogger.h \
    src/utils.h

win32: {
LIBS += -luser32
RC_ICONS = img/exeye.ico
}
