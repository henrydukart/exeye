#include "filelogger.h"

#include <QDateTime>
#include <QTextStream>


FileLogger::FileLogger()
{
    m_file.setFileName(QStringLiteral("log"));
    m_file.open(QIODevice::WriteOnly | QIODevice::Text);
}

void FileLogger::log(const QString &msg)
{
    if (msg.startsWith(QStringLiteral("requestActivate() called for")))
        return;
    QString outputString = QDateTime::currentDateTime().toString(Qt::ISODate) + " | " + msg + "\n";
    QTextStream(&m_file) << outputString;
    m_file.flush();
#ifdef QT_DEBUG
    fprintf(stderr, "%s", outputString.toLatin1().constData());
#endif

}
