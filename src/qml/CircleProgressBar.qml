import QtQuick 2.0
import QtQuick.Controls.Material 2.2

Item {
    width: 100
    height: width

    property real from: 0
    property real to: 1
    property real value: 0
    property var color: Material.accent
    property int lineWidth: 4
    property string text: ""

    onFromChanged: canvas.requestPaint()
    onToChanged: canvas.requestPaint()
    onValueChanged: canvas.requestPaint()
    onTextChanged: canvas.requestPaint()
    onLineWidthChanged: canvas.requestPaint()
    onColorChanged: canvas.requestPaint()

    Canvas {
        id: canvas
        anchors.fill: parent
        antialiasing: true
        smooth: true

        onPaint: {
            var radius = Math.min(width, height) / 2 - parent.lineWidth / 2
            var halfPI = Math.PI / 2
            var ctx = getContext("2d")
            ctx.clearRect(0, 0, width, height)
            ctx.save()

            ctx.translate(width / 2, height / 2)
            ctx.lineWidth = parent.lineWidth

            ctx.beginPath()
            ctx.strokeStyle = Qt.darker(parent.color)
            ctx.arc(0, 0, radius, 0, Math.PI * 2, false)
            ctx.stroke()

            ctx.beginPath()
            ctx.strokeStyle = parent.color
            ctx.arc(0, 0, radius, halfPI,
                    parent.value / (parent.to - parent.from) * 2 * Math.PI + halfPI, false)
            ctx.stroke()

            ctx.beginPath()
            var fontSize = 48
            ctx.fillStyle = parent.color
            ctx.textAlign = "center"
            ctx.textBaseline = "middle"
            ctx.font = fontSize + "px Arial"
            ctx.fillText(parent.text, 0, 0)
            ctx.fill()

            ctx.restore()
        }
    }
}
