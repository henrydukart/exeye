import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3
import QtCharts 2.3
import Qt.custom 1.0


Item {
    property string currentState
    property bool userActive
    property bool enableDarkTheme

    onUserActiveChanged: updateChart()
    onCurrentStateChanged: updateChart()

    function updateChart() {
        if (currentState === "")
            return
        if (attr.pieToUpdate === "") {
            attr.pieToUpdate = attr.getLabelFromState(userActive, currentState)
            return
        }
        var changedTime = attr.getTime()
        var duration = changedTime - attr.prevChangedTime
        attr.prevChangedTime = changedTime

        pieSeries.find(attr.pieToUpdate).value += duration
        attr.pieToUpdate = attr.getLabelFromState(userActive, currentState)
    }

    QtObject {
        id: attr
        function getTime() { return (new Date()).getTime() }
        function getLabelFromState(usAct, st) {
            if (!usAct && st !== "longBreak")
                return qsTr("Inactive")
            switch (st){
            case "postpone":
            case "work": return qsTr("Work")
            case "longBreak":
            case "shortBreak": return qsTr("Break")
            case "stopped": return qsTr("Stopped")
            case "stoppedInactive": return qsTr("Inactive")
            }
        }

        property var prevChangedTime: getTime()
        property string pieToUpdate: ""
    }

    ChartView {
        id: chart
        anchors.left: parent.left
        anchors.top: parent.top
        legend.visible: false
        antialiasing: true
        smooth: true
        theme: enableDarkTheme ? ChartView.ChartThemeDark : ChartView.ChartThemeLight
        backgroundColor: enableDarkTheme ? "transparent" : "transparent"
        animationDuration: 1000
        animationOptions: ChartView.SeriesAnimations
        clip: true
        height: 200
        width: 200

        PieSeries {
            id: pieSeries
            size: 0.95
            holeSize: 0.7
            onHovered: centerLabel.text = state ? slice.label : ""
            PieSlice { id: workSlice; label: qsTr("Work"); value: 1 }
            PieSlice { id: breakSlice; label: qsTr("Break"); value: 0 }
            PieSlice { id: inactiveSlice; label: qsTr("Inactive"); value: 0 }
            PieSlice { id: stoppedSlice; label: qsTr("Stopped"); value: 0 }
        }
    }

    Timer {
        running: true
        interval: 5000
        repeat: true
        onTriggered: updateChart()
    }
    Label { id: centerLabel; anchors.centerIn: chart }

    GridLayout {
        id: legend
        anchors.right: parent.right
        anchors.verticalCenter: chart.verticalCenter
        columns: 3
        columnSpacing: 20
        Rectangle { width: 10; height: width; radius: width / 2; color: workSlice.color }
        Label { text: workSlice.label }
        Label { text: Utils.milliSecsToString(workSlice.value) }

        Rectangle { width: 10; height: width; radius: width / 2; color: breakSlice.color }
        Label { text: breakSlice.label }
        Label { text: Utils.milliSecsToString(breakSlice.value) }

        Rectangle { width: 10; height: width; radius: width / 2; color: inactiveSlice.color }
        Label { text: inactiveSlice.label }
        Label { text: Utils.milliSecsToString(inactiveSlice.value) }

        Rectangle { width: 10; height: width; radius: width / 2; color: stoppedSlice.color }
        Label { text: stoppedSlice.label }
        Label { text: Utils.milliSecsToString(stoppedSlice.value) }
    }
}
