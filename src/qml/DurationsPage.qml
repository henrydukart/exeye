import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.1

Item {
    property alias workingIntervalValue: workingInterval.value
    property alias longBreakDurationValue: longBreakDuration.value
    property alias shortBreakFrequencyValue: shortBreakFrequency.value
    property alias postponeIntervalValue: postponeInterval.value

    GridLayout {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: 50
        }

        columns: 3

        Label { text: qsTr("Working time") }
        Slider {
            id: workingInterval
            from: 30
            to: 60
            stepSize: 5
            Layout.fillWidth: true
        }
        Label { text: workingInterval.value }

        Label { text: qsTr("Long break duration") }
        Slider {
            id: longBreakDuration
            from: 10
            to: 15
            stepSize: 1
            Layout.fillWidth: true
        }
        Label { text: longBreakDuration.value }

        Label { text: qsTr("Short breaks frequency") }
        Slider {
            id: shortBreakFrequency
            from: 5
            to: 15
            stepSize: 5
            Layout.fillWidth: true
        }
        Label { text: shortBreakFrequency.value }

        Label { text: qsTr("Postpone time") }
        Slider {
            id: postponeInterval
            from: 1
            to: 3
            stepSize: 1
            Layout.fillWidth: true
        }
        Label { text: postponeInterval.value }

        Settings {
            property alias workingIntervalValue: workingInterval.value
            property alias longBreakDurationValue: longBreakDuration.value
            property alias shortBreakFrequencyValue: shortBreakFrequency.value
            property alias postponeIntervalValue: postponeInterval.value
        }
    }
}
