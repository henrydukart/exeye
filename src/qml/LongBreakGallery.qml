import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.1
import Qt.custom 1.0


Item {

    property var imagePathes: []

    function updateImagePathes(){
        var tmpArray = []
        for (var i = 0; i < imageModel.count; ++i)
            tmpArray.push(imageModel.get(i).path)
        imagePathes = tmpArray
    }

    ListModel { id: imageModel }

    GridView {
        id: view
        anchors.fill: parent
        anchors.leftMargin: 8
        anchors.rightMargin: 8

        model: imageModel
        delegate: imageDelegate

        clip: true
        cellWidth: 100
        cellHeight: 100

        displaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 200 }
        }

        FileDialog {
            id: fileDialog
            title: qsTr("Choose images")
            folder: shortcuts.pictures
            selectMultiple: true
            nameFilters: [ qsTr("Image files") + "(*.jpg *.png)" ]
            onAccepted: {
                fileUrls.forEach(function(path){imageModel.append({"path": path})})
                updateImagePathes()
            }
        }

        RoundButton {
            x: imageModel.count > 0 ? view.width - width : 0
            y: imageModel.count > 0 ? view.height - height : 0

            text: "+"
            onClicked: fileDialog.visible = true
            Behavior on x { NumberAnimation { duration: 250 }}
            Behavior on y { NumberAnimation { duration: 250 }}
        }

        ScrollIndicator.vertical: ScrollIndicator { }
    }

    Component {
       id: imageDelegate

       Image {
           id: img
           source: model.path
           width: GridView.view.cellWidth
           height: GridView.view.cellHeight
           fillMode: Image.PreserveAspectFit

           RoundButton {
               anchors { right: parent.right; bottom: parent.bottom }

               width: 40
               height: 40
               text: "\u00D7"
               onClicked: {
                   imageModel.remove(index)
                   updateImagePathes()
               }
           }

           GridView.onRemove: SequentialAnimation {
               PropertyAction {
                   target: img
                   property: "GridView.delayRemove"
                   value: true
               }
               NumberAnimation {
                   target: img
                   property: "scale"
                   to: 0
                   duration: 200
                   easing.type: Easing.InOutQuad
               }
               PropertyAction { target: img
                   property: "GridView.delayRemove"
                   value: false
               }
           }

           GridView.onAdd: SequentialAnimation {
               NumberAnimation { target: img
                   property: "scale"
                   from: 0; to: 1
                   duration: 200
                   easing.type: Easing.InOutQuad
               }
           }
       }
    }

    Settings { id: settings }

    Component.onCompleted: {
        settings.value("imagePathes", []).forEach(function(path){
            if (Utils.isFileExists(path.substring(8)))
                imageModel.append({"path": path})
        })
        updateImagePathes()

    }

    Component.onDestruction: {
        var pathes = []
        for (var i = 0; i < imageModel.count; ++i)
            pathes.push(imageModel.get(i).path)
        settings.setValue("imagePathes", pathes)
    }
}
