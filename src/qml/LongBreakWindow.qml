import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.custom 1.0

import "imageSplashScreenFunctions.js" as SplashScreen

ApplicationWindow {

    id: root

    property int passwordHash
    property var allowSkip
    property alias durationSeconds: progress.to
    property bool useImages
    property var imagePathes

    height: layout.implicitHeight + 40
    width: layout.implicitWidth + 40
    flags: Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint
    x: -(width + 50)

    signal skipClicked

    function showLongBreakWindow() {
        y = screen.desktopAvailableHeight - (height + 20)
        progress.value = 0
        var pathes = useImages ? imagePathes : []
        SplashScreen.showSplash(parent, pathes)
        showAnimation.start()
    }

    function hideLongBreakWindow(){
        hideAnimation.start()
        SplashScreen.hideSplash()
    }

    SequentialAnimation {
        id: showAnimation
        ScriptAction { script: root.show() }
        SpringAnimation { target: root; property: "x"; spring: 2; damping: 0.2
            to: screen.desktopAvailableWidth - (root.width + 30)
        }
        onStopped: {
            raise()
            forceActiveFocus()
            requestActivate()
        }
    }

    SequentialAnimation {
        id: hideAnimation
        SpringAnimation { target: root; property: "x"; spring: 2; damping: 0.2;
            to: -root.width - 50
        }
        ScriptAction { script: root.hide() }
    }


    SequentialAnimation {
        id: shakeAnimation
        loops: 4
        NumberAnimation { target: root; property: "x"; to: root.x - 20; duration: 150 }
        NumberAnimation { target: root; property: "x"; to: root.x; duration: 150 }
    }

    Timer {
        running: visible
        repeat: true
        triggeredOnStart: true
        interval: 1000
        onTriggered: {
            progress.value += 1
            var secondsLeft = progress.to - progress.value
            if (secondsLeft < 0) {
                stop()
                return
            }
            var mins = Math.floor(secondsLeft / 60)
            var secs = secondsLeft - mins * 60
            progress.text = (mins < 10 ? "0" + mins : mins) + ":" +
                    (secs < 10 ? "0" + secs : secs)
        }
    }

    GridLayout {
        anchors.centerIn: parent
        columns: 2
        rows: 3
        id: layout

        CircleProgressBar {
            id: progress
            width: 200
            Layout.rowSpan: 3
        }

        Label {
            text: "<h1>" + qsTr("Long break") + "</h1>" +
                  qsTr("Take a rest")

            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.rowSpan: allowSkip === LongBreakPage.SkipLongBreak.Password ? 1 :
                            allowSkip === LongBreakPage.SkipLongBreak.Allow ? 2 : 3
        }

        TextField {
            id: password
            echoMode: TextInput.Password
            visible: allowSkip === LongBreakPage.SkipLongBreak.Password
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom
        }

        Button {
            id: button
            text: qsTr("Skip")
            flat: true
            onClicked: {
                if (allowSkip === LongBreakPage.SkipLongBreak.Allow){
                    skipClicked()
                    return
                }

                if (passwordHash === Utils.hash(password.text)) {
                    password.clear()
                    skipClicked()
                }
                else {
                    password.focus = true
                    shakeAnimation.start()
                }
            }

            visible: allowSkip !== LongBreakPage.SkipLongBreak.Forbid
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignBottom
        }
    }
}
