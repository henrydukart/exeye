import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import Qt.custom 1.0

Item {
    id: root
    property alias currentState: spentTimeChart.currentState
    property alias userActive: spentTimeChart.userActive
    property bool enableDarkTheme

    readonly property real startTime: Date.now()

    onCurrentStateChanged: {
        var label = null
        if (currentState === "longBreak")
            label = longBreaksLabel
        else if (currentState === "shortBreak")
            label = shortBreaksLabel

        if (label) {
            label.text = parseInt(label.text, 10) + 1
        }
    }

    Column {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: 50
        }
        spacing: 20

        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            spacing: 10

            Label { text: qsTr("General info") }
            GridLayout {
                columns: 2
                anchors.right: parent.right
                anchors.left: parent.left

                Label { text: qsTr("Start time"); leftPadding: 10 }
                Label { text: (new Date(startTime)).toLocaleString(); Layout.alignment: Qt.AlignRight | Qt.AlignVCenter  }

                Label { text: qsTr("Total elapsed time"); leftPadding: 10 }
                Label {
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Timer {
                        interval: 5000
                        repeat: true
                        running: true
                        triggeredOnStart: true
                        onTriggered: parent.text = Utils.milliSecsToString(Date.now() - startTime)
                    }
                }

                Label { text: qsTr("Number of long breaks"); leftPadding: 10 }
                Label { id: longBreaksLabel; text: "0"; Layout.alignment: Qt.AlignRight | Qt.AlignVCenter }

                Label { text: qsTr("Number of short breaks"); leftPadding: 10 }
                Label { id: shortBreaksLabel; text: "0"; Layout.alignment: Qt.AlignRight | Qt.AlignVCenter }
            }
        }

        Column {
            anchors.left: parent.left
            anchors.right: parent.right
            Label { text:qsTr("Spent time") }
            SpentTimeChart {
                anchors.left: parent.left
                anchors.right: parent.right
                id: spentTimeChart
                enableDarkTheme: root.enableDarkTheme
            }
        }
    }
}
