import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.3
import Qt.labs.settings 1.1
import Qt.custom 1.0

Item {

    id: page

    enum SkipLongBreak {
        Allow,
        Forbid,
        Password
    }

    property var allowSkip: LongBreakPage.SkipLongBreak.Allow
    property alias useImages: gallerySwitch.checked
    property alias imagePathes: gallery.imagePathes
    property alias soundVolume: volumeSlider.value
    property int passwordHash: 0

    property bool passwordSetted: passwordHash !== 0

    function clearAllFields(){
        oldPwd.clear()
        newPwd.clear()
        confirmPwd.clear()
        warningLabel.text = ""
    }

    Drawer {
        id: changePasswordDrawer
        modal: true
        interactive: false

        width: page.width * 0.5
        height: page.height

        position: 0
        visible: false

        edge: Qt.RightEdge

        ColumnLayout {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: 8
            }

            Label { text: qsTr("Old password"); visible: passwordSetted }
            TextField {
                id: oldPwd
                selectByMouse: true
                focus: true
                Layout.fillWidth: true
                echoMode: TextInput.Password
                visible: passwordSetted
            }
            Label { text: qsTr("New password") }
            TextField {
                id: newPwd
                selectByMouse: true
                Layout.fillWidth: true
                echoMode: TextInput.Password
            }
            Label { text: qsTr("Confirm password") }
            TextField {
                id: confirmPwd
                selectByMouse: true
                Layout.fillWidth: true
                echoMode: TextInput.Password
            }
            Label { id: warningLabel; color: Material.accent }
            RowLayout {
                Layout.alignment: Qt.AlignRight

                Button {
                    text: qsTr("Change")
                    flat: true
                    onClicked: {
                        if (passwordSetted && Utils.hash(oldPwd.text) !== passwordHash) {
                            warningLabel.text = qsTr("Old password is incorrect")
                            oldPwd.focus = true
                            return
                        }

                        if (newPwd.text.length == 0) {
                            warningLabel.text = qsTr("New password can not be empty")
                            newPwd.focus = true
                            return
                        }

                        if (newPwd.text !== confirmPwd.text) {
                            warningLabel.text = qsTr("New passwords do not match")
                            confirmPwd.focus = true
                            return
                        }

                        passwordHash = Utils.hash(newPwd.text)
                        clearAllFields()
                        changePasswordDrawer.close()
                    }
                }


                Button {
                    text: qsTr("Close")
                    flat: true
                    onClicked: {
                        clearAllFields()
                        changePasswordDrawer.close()
                    }
                }
            }
        }
    }

    Drawer {
        id: checkPasswordDrawer
        modal: true
        interactive: false

        width: page.width * 0.5
        height: page.height

        position: 0
        visible: false

        edge: Qt.RightEdge

        ColumnLayout {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: 8
            }

            Label { text: qsTr("Password") }
            TextField {
                id: checkPwd
                selectByMouse: true
                Layout.fillWidth: true
                echoMode: TextInput.Password
            }
            Label { id: matchLabel; color: Material.accent }
            RowLayout {
                Layout.alignment: Qt.AlignRight

                Button {
                    focus: true
                    flat: true
                    text: qsTr("Enter")
                    onClicked: {
                        if (Utils.hash(checkPwd.text) !== passwordHash) {
                            matchLabel.text = qsTr("Password is incorrect")
                            checkPwd.focus = true
                            return
                        }

                        checkPwd.clear()
                        matchLabel.text = ""
                        checkPasswordDrawer.close()
                    }
                }


                Button {
                    flat: true
                    text: qsTr("Close")
                    onClicked: {
                        blockSwitch.checked = true
                        checkPwd.clear()
                        matchLabel.text = ""
                        checkPasswordDrawer.close()
                    }
                }
            }
        }
    }

    ColumnLayout {

        anchors {
            fill: parent
            margins: 50
        }

        Label { text: qsTr("Skip long breaks") }
        ColumnLayout {
            Layout.fillWidth: true

            Row {
                ButtonGroup { buttons: parent.children }

                RadioButton {
                    id: allowRadio
                    text: qsTr("Allow");
                    checked: allowSkip === LongBreakPage.SkipLongBreak.Allow;
                    onClicked: allowSkip = LongBreakPage.SkipLongBreak.Allow
                    enabled: !blockSwitch.checked
                }
                RadioButton {
                    id: forbidRadio
                    text: qsTr("Forbid")
                    checked: allowSkip === LongBreakPage.SkipLongBreak.Forbid
                    onClicked: allowSkip = LongBreakPage.SkipLongBreak.Forbid
                    enabled: !blockSwitch.checked
                }
                RadioButton {
                    id: passwordRadio
                    text: qsTr("Allow with password")
                    checked: allowSkip === LongBreakPage.SkipLongBreak.Password
                    onClicked: allowSkip = LongBreakPage.SkipLongBreak.Password
                    enabled: !blockSwitch.checked && passwordSetted
                }
            }
            Switch {
                id: blockSwitch
                text: qsTr("Block options with password") + (!passwordSetted ? qsTr(" (password is not setted)") : "")
                checked: false
                enabled: passwordSetted
                onCheckedChanged: {
                    if (!checked)
                        checkPasswordDrawer.open()
                }
            }
            Button {
                flat: true
                Layout.leftMargin: 8
                text: qsTr("Change password")
                onClicked: changePasswordDrawer.open()
            }
        }

        RowLayout {
            Layout.rightMargin: 8
            Layout.fillWidth: true
            Label { text: qsTr("Sound volume") }
            Slider {
                id: volumeSlider
                value: 1
                stepSize: 0.1
                Layout.fillWidth: true
            }
        }

        Switch {
            id: gallerySwitch
            checked: true
            text: qsTr("Use background wallpapers")
        }
        Label { text: qsTr("Background wallpapers") }
        LongBreakGallery {
            id: gallery
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 100
            Layout.minimumHeight: 100
        }
    }

    Settings {
        id: settings
        property alias allowSkip: page.allowSkip
        property alias blockOptions:blockSwitch.checked
        property alias passwordHash: page.passwordHash
        property alias useImages: gallerySwitch.checked
        property alias soundVolume: volumeSlider.value
    }

}
