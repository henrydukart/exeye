import QtQuick 2.0

Item {
    property real interval: 0
    property alias repeat: timer.repeat

    function remainingTime() {
        if (!attr.started)
            return interval
        if (!timer.running)
            return interval - attr.elapsedTime
        return interval - attr.elapsedTime -
                (attr.getTime() - attr.lastResumedTime)
    }

    function start() {
        if (timer.running)
            return
        if (!attr.started) {
            attr.started = true
            attr.elapsedTime = 0
        }
        attr.lastResumedTime = attr.getTime()
        timer.interval = Math.max(interval - attr.elapsedTime, 1)
        timer.start()
    }

    function pause() {
        if (!attr.started)
            return
        attr.elapsedTime += attr.getTime() - attr.lastResumedTime
        timer.stop()
    }

    function stop() {
        if (!attr.started)
            return
        attr.started = false
        timer.stop()
    }

    onIntervalChanged: {
        if (!attr.started || !timer.running)
            return
        pause()
        start()
    }

    signal triggered

    QtObject {
        id: attr
        property bool started: false
        property real elapsedTime
        property real lastResumedTime
        function getTime() { return (new Date().getTime()) }
    }

    Timer {
        id: timer
        onTriggered: {
            if (!repeat)
                attr.started = false
            interval = parent.interval
            parent.triggered()
        }
    }
}
