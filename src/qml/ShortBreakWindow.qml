import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "imageSplashScreenFunctions.js" as SplashScreen

ApplicationWindow {

    property alias durationSeconds: progress.to
    property bool showSplash: true

    id: root
    height: 250
    width: 350
    visible: false
    flags: Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint

    function showShortBreakWindow() {
        if (visible)
            return
        var exIndex = Math.floor(Math.random() * Math.floor(exercises.length))
        image.source = exercises[exIndex].image
        description.text = exercises[exIndex].description
        progress.value = 0
        x = (screen.width - width) / 2
        y = -(height + 50)
        if (showSplash)
            SplashScreen.showSplash(parent, [])
        showAnimation.start()
    }

    function hideShortBreakWindow() {
        if (!visible)
            return
        hideAnimation.start()
        if (showSplash)
            SplashScreen.hideSplash()
    }

    SequentialAnimation {
        id: showAnimation
        ScriptAction { script: root.show() }
        SpringAnimation { target: root; property: "y"; spring: 2; damping: 0.2
            to: (screen.height - root.height) / 2
        }
    }

    SequentialAnimation {
        id: hideAnimation
        SpringAnimation { target: root; property: "y"; spring: 2; damping: 0.2;
            to: -(root.height + 50)
        }
        ScriptAction { script: root.hide() }
    }

    Timer {
        running: visible
        repeat: true
        interval: 1000
        onTriggered: progress.value++
    }

    ColumnLayout {
        anchors.fill: parent

        Label { text: qsTr("Short break"); Layout.alignment: Qt.AlignHCenter }
        Image {
            id: image
            smooth: true
            antialiasing: true
            Layout.alignment: Qt.AlignHCenter
        }

        Label {
            id: description
            width: root.width - 20
            wrapMode: Label.WordWrap
            Layout.alignment: Qt.AlignHCenter
        }

        ProgressBar {
            id: progress
            from: 0
            to: durationSeconds
            value: 0

            Layout.fillWidth: true
        }

    }

    readonly property var exercises: [
        {
            description: qsTr("Move your eyes left and right"),
            image: "qrc:/img/exercise/left-right.png"
        },
        {
            description: qsTr("Move your eyes up and down"),
            image: "qrc:/img/exercise/up-down.png"
        },
        {
            description: qsTr("Rotate your eyes"),
            image: "qrc:/img/exercise/round.png"
        },
        {
            description: qsTr("Look at the tip of your nose and forward"),
            image: "qrc:/img/exercise/nose.png"
        },
        {
            description: qsTr("Open your eyes wide and close with force"),
            image: "qrc:/img/exercise/blink.png"
        },
        {
            description: qsTr("Move your eyes diagonally"),
            image: "qrc:/img/exercise/diagonal.png"
        }
    ]
}
