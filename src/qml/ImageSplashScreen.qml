import QtQuick 2.0
import QtQuick.Controls 2.2

ApplicationWindow {
    id: splash

    flags: Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint |
           Qt.WindowDoesNotAcceptFocus | Qt.WA_TranslucentBackground
    color: "transparent"

    property var images

    background: Rectangle {
        id: bgRect
        color: "black"
        opacity: 0
    }

    Image {
        id: img
        anchors.centerIn: parent
        height: parent.height
        width: parent.width
        fillMode: Image.PreserveAspectFit
        scale: 0
    }

    Timer {
        id: timer
        interval: 60 * 1000
        repeat: true
        triggeredOnStart: true
        property int curIndex: 0
        onTriggered: {
            if (curIndex === images.length)
                curIndex = 0
            img.source = images[curIndex]
            ++curIndex
        }
    }

    function showSplash(imagePathes){
        images = imagePathes
        img.visible = false
        timer.curIndex = 0
        showAnimation.start()
        if (imagePathes.length > 0) {
            img.visible = true
            timer.start()
        }
    }

    function hideSplash() {
        timer.stop()
        hideAnimation.start()
    }

    SequentialAnimation {
        id: showAnimation
        PropertyAction { target: splash; property: "visibility"; value: 5 }
        NumberAnimation { target: bgRect; property: "opacity"; to: 0.8; duration: 1000 }
        NumberAnimation { target: img; property: "scale"; to: 1; duration: 200}
    }

    SequentialAnimation {
        id: hideAnimation
        NumberAnimation { target: img; property: "scale"; to: 0; duration: 200}
        NumberAnimation { target: bgRect; property: "opacity"; to: 0; duration: 1000 }
        PropertyAction { target: splash; property: "visibility"; value: 0 }
    }
}
