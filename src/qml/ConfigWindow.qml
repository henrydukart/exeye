import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3


ApplicationWindow {

    id: applicationWindow
    visible: true
    width: 720
    height: 480
    title: "Exeye"
    flags: Qt.Dialog

    Material.theme: otherOptions.enableDarkTheme ? Material.Dark : Material.Light

    Drawer {
        id: drawer

        width: applicationWindow.width / 4
        height: applicationWindow.height

        modal: false
        interactive: false
        position: 1
        visible: true

        ListView {
            id: listView
            anchors.fill: parent

            model: [qsTr("Durations"), qsTr("Long break"), qsTr("Other options"),
                    qsTr("Statistic"), qsTr("About")]

            delegate: ItemDelegate {
                text: modelData
                highlighted: ListView.isCurrentItem
                width: parent.width
                onClicked: {
                    listView.currentIndex = index
                    swipeView.currentIndex = index
                }
            }

            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        anchors.leftMargin: drawer.width
        interactive: false
        orientation: Qt.Vertical

        DurationsPage {
            id: durations
        }

        LongBreakPage {
            id: longBreak
        }

        OtherOptionsPage {
            id: otherOptions
        }

        StatisticsPage {
            currentState: stateAndLogic.state
            userActive: stateAndLogic.userActive
            enableDarkTheme: otherOptions.enableDarkTheme
        }

        AboutPage {
        }
    }

    StatesLogics {
        id: stateAndLogic

        Component.onCompleted: start()

        applicationWindow: applicationWindow
        workInterval: durations.workingIntervalValue
        longBreakInterval: durations.longBreakDurationValue
        shortBreakFreqInterval: durations.shortBreakFrequencyValue
        postponeInterval: durations.postponeIntervalValue

        allowSkipLongBreak: longBreak.allowSkip
        passwordHash: longBreak.passwordHash
        useImages: longBreak.useImages
        imagePathes: longBreak.imagePathes
        soundVolume: longBreak.soundVolume

        showShortBreakSplash: otherOptions.showShortBreakSplash
        remindWhenStopped: otherOptions.remindStopped
        enableActivityMonitor: otherOptions.enableActivityMonitor
        enableDarkTheme: otherOptions.enableDarkTheme
    }

    onClosing: {
        close.accepted = false
        applicationWindow.hide()
    }
}
