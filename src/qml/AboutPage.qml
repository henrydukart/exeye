import QtQuick 2.0
import QtQuick.Controls 2.2

Item {
    Row {
        anchors.centerIn: parent
        anchors.margins: 50
        spacing: 20

        Image {
            source: "qrc:/img/trayicon.png"
        }
        Label {
            text: "<h1>Exeye 1.0</h1> Popov Denis<br>henry_dukart@mail.ru"
        }
    }
}
