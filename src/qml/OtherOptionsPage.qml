import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Qt.labs.settings 1.1

Item {

    property alias showShortBreakSplash: showShortBreakSplashSwitch.checked
    property alias remindStopped: stoppedReminderSwitch.checked
    property alias enableActivityMonitor: enableActivityMonitorSwitch.checked
    property alias enableDarkTheme: enableDarkThemeSwitch.checked

    ColumnLayout {

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: 50
        }

        Switch {
            id: showShortBreakSplashSwitch
            checked: true
            text: qsTr("Show short break window full screen")
        }
        Switch {
            id: stoppedReminderSwitch
            checked: true
            text: qsTr("Remind every 10 minutes when Exeye is stopped")
        }
        Switch {
            id: enableActivityMonitorSwitch
            checked: true
            text: qsTr("Enable activity monitoring")
        }
        Switch {
            id: enableDarkThemeSwitch
            checked: true
            text: qsTr("Enable dark theme")
        }
    }

    Settings {
        property alias showSplash: showShortBreakSplashSwitch.checked
        property alias remindStopped: stoppedReminderSwitch.checked
        property alias enableActivityMonitor: enableActivityMonitorSwitch.checked
        property alias enableDarkTheme: enableDarkThemeSwitch.checked
    }
}
