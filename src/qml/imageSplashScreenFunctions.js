var splashScreens = []

function showSplash(parent, imagePathes) {
    if (splashScreens.length === 0) {
        var component = Qt.createComponent("ImageSplashScreen.qml")
        for (var i = 0; i < Qt.application.screens.length; ++i) {
            splashScreens.push(
                        component.createObject(parent, {screen: Qt.application.screens[i]}))
        }
    }

    splashScreens.forEach(function(splash){
        splash.showSplash(imagePathes)
    })
}

function hideSplash() {
    splashScreens.forEach(function(splash){
        splash.hideSplash()
    })
}

function destroy() {
    splashScreens.forEach(function(splash){
        splash.destroy()
    })
}
