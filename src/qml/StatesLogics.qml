import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import Qt.labs.platform 1.0
import Qt.custom 1.0
import QtMultimedia 5.12
import QtQuick.Controls.Material 2.3

import "imageSplashScreenFunctions.js" as SplashScreen

Item {

    property var applicationWindow
    property int workInterval
    property int longBreakInterval
    property int shortBreakFreqInterval
    property int postponeInterval

    property alias passwordHash: longBreakWindow.passwordHash
    property alias allowSkipLongBreak: longBreakWindow.allowSkip
    property alias useImages: longBreakWindow.useImages
    property alias imagePathes: longBreakWindow.imagePathes
    property real soundVolume

    property alias showShortBreakSplash: shortBreakWindow.showSplash
    property bool remindWhenStopped
    property bool enableActivityMonitor
    property bool enableDarkTheme

    readonly property int shortBreakDuration: 15
    property bool postponed: false

    property alias userActive: activityMonitor.active

    function start() {
        state = "work"
    }

    function stopFor(minutes) {
        state = "stopped"
        stopTimer.interval = minutes * 60 * 1000
    }

    function startInactive() {
        state = "stoppedInactive"
    }

    function stopAllTimers() {
        stopShortBreak()
        shortBreakFreqTimer.stop()
        stopLongBreak()
        workTimer.stop()
    }

    Component.onDestruction: SplashScreen.destroy()

    onStateChanged: print(state)

    ActivityMonitor {
        id: activityMonitor
        timeout: 5 * 1000 * 60
        enabled: state !== "stopped" && enableActivityMonitor
        onActiveChanged: {
            if (state === "stoppedInactive" && active) {
                print("activated")
                start()
            }
            else if (!active)
                print("inactive for 5 minutes")
        }
    }

    Timer {
        interval: (longBreakInterval * 3 / 2) * 1000 * 60
        running: !activityMonitor.active
        onTriggered: startInactive()
    }

    ShortBreakWindow {
        id: shortBreakWindow
        durationSeconds: shortBreakDuration
        screen: Qt.application.screens[0]
        Material.theme: otherOptions.enableDarkTheme ? Material.Dark : Material.Light
    }

    LongBreakWindow {
        id: longBreakWindow
        screen: Qt.application.screens[0]
        durationSeconds: longBreakInterval * 60
        onSkipClicked: start()
        Material.theme: otherOptions.enableDarkTheme ? Material.Dark : Material.Light
    }

    PostponeWindow {
        id: postponeWindow
        screen: Qt.application.screens[0]
        durationSeconds: 30
        Material.theme: otherOptions.enableDarkTheme ? Material.Dark : Material.Light
        onPostponeClicked: {
            postponed = true
            start()
        }
    }

    PauseTimer {
        id: workTimer
        interval: (!postponed ? workInterval : postponeInterval) * 1000 * 60
        onTriggered: parent.state = "postpone"
    }
    Timer {
        id: longBreakTimer
        interval: longBreakInterval * 1000 * 60
        onTriggered: parent.state = "work"
    }
    PauseTimer {
        id: shortBreakFreqTimer
        interval: shortBreakFreqInterval * 1000 * 60
        repeat: true
        onTriggered: if (workTimer.remainingTime() >= interval) parent.state = "shortBreak"
    }
    Timer {
        id: shortBreakTimer
        interval: shortBreakDuration * 1000
        onTriggered: parent.state = "work"
    }
    Timer {
        id: stopTimer
        onTriggered: parent.state = "work"
    }
    Timer {
        id: postponeTimer
        interval: 30 * 1000
        onTriggered: parent.state = "longBreak"
    }

    function stopLongBreak() {
        longBreakWindow.hideLongBreakWindow()
        longBreakTimer.stop()
    }

    function stopShortBreak() {
        shortBreakWindow.hideShortBreakWindow()
        shortBreakTimer.stop()
    }

    function stopPostpone() {
        postponeWindow.hidePostponeWindow()
        postponeTimer.stop()
    }

    states: [
        State {
            name: "work"
            StateChangeScript {
                script: {
                    stopPostpone()
                    stopShortBreak()
                    stopLongBreak()
                    workTimer.start()
                    shortBreakFreqTimer.start()
                }
            }
        },
        State {
            name: "shortBreak"
            StateChangeScript {
                script: {
                    workTimer.pause()
                    shortBreakTimer.start()
                    shortBreakFreqTimer.stop()
                    shortBreakWindow.showShortBreakWindow()
                }
            }
        },
        State {
            name: "longBreak"
            StateChangeScript {
                script: {
                    stopPostpone()
                    postponed = false
                    stopShortBreak()
                    shortBreakFreqTimer.stop()
                    workTimer.stop()
                    longBreakTimer.start()
                    longBreakWindow.showLongBreakWindow()
                }
            }
        },
        State {
            name: "stopped"
            StateChangeScript {
                script: {
                    stopAllTimers()
                    stopTimer.start()
                }
            }
        },
        State {
            name: "stoppedInactive"
            StateChangeScript {
                script: {
                    stopAllTimers()
                    tray.showMessage(applicationWindow.title,
                                         qsTr("You was inactive for a long time."))
                }
            }
        },
        State {
            name: "postpone"
            StateChangeScript {
                script: {
                    shortBreakFreqTimer.stop()
                    postponeTimer.start()
                    postponeWindow.showPostponeWindow()
                }
            }
        }

    ]

    SoundEffect {
        id: sound
        source: "qrc:/sounds/bell.wav"
        volume: QtMultimedia.convertVolume(soundVolume,
                                           QtMultimedia.LogarithmicVolumeScale,
                                           QtMultimedia.LinearVolumeScale)
    }

    transitions: [
        Transition {
            from: "longBreak"
            to: "*"
            ScriptAction { script: sound.play() }
        },
        Transition {
            from: "stoppedInactive"
            to: "*"
            ScriptAction { script: tray.showMessage(applicationWindow.title,
                                                    qsTr("You've returned after a long break."))}
        }
    ]

    Timer {
        running: remindWhenStopped && state === "stopped"
        repeat: true
        interval: 10 * 1000 * 60
        onTriggered: tray.showMessage(applicationWindow.title, qsTr("Exeye is stopped."))
    }

    Timer {
        interval: 1000
        repeat: true
        running: true
        triggeredOnStart: true

        onTriggered: {
            if (state === "stopped")
                tray.tooltip = qsTr("Stopped")
            else if (state === "postpone")
                tray.tooltip = qsTr("Postpone")
            else {
                tray.tooltip = Utils.milliSecsToString(workTimer.remainingTime()) + " " +
                        qsTr("working time left")
                if (state !== "shortBreak")
                    tray.tooltip += "\n" + Utils.milliSecsToString(shortBreakFreqTimer.remainingTime()) + " " +
                            qsTr("before short break")
            }
        }
    }

    SystemTrayIcon {
        visible: true
        iconSource: state === "stopped" ? "qrc:/img/stoppedtrayicon.png" : "qrc:/img/trayicon.png"
        id: tray

        menu: Menu {
            MenuItem {
                text: qsTr("Take a long break")
                onTriggered: state = "longBreak"
            }
            MenuItem {
                enabled: state === "stopped"
                text: qsTr("Start")
                onTriggered: start()
            }

            Menu {
                title: qsTr("Stop for ...")
                enabled: state !== "stopped"
                MenuItem {
                    text: qsTr("30 minutes")
                    onTriggered: stopFor(30)
                }
                MenuItem {
                    text: qsTr("1 hour")
                    onTriggered: stopFor(60)
                }
            }

            MenuSeparator { }
            MenuItem {
                text: qsTr("Quit")
                onTriggered: Qt.quit()
            }
        }

        Component.onCompleted: {
            showMessage(applicationWindow.title, qsTr("Exeye was started."))
        }

        onActivated: {
            if (reason === SystemTrayIcon.Context)
                return
            applicationWindow.show()
            applicationWindow.raise()
            applicationWindow.requestActivate()
        }
    }
}
