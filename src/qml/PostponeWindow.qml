import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {

    id: root
    height: layout.implicitHeight + 40
    width: layout.implicitWidth + 40
    x: screen.desktopAvailableWidth - (width + 20)
    visible: false

    property alias durationSeconds: progress.to
    signal postponeClicked

    flags: Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint

    function showPostponeWindow() {
        if (visible)
            return
        y = screen.desktopAvailableHeight + (height + 20)
        progress.value = 0
        showAnimation.start()
    }

    function hidePostponeWindow(){
        if (visible)
            hideAnimation.start()
    }

    SequentialAnimation {
        id: showAnimation
        ScriptAction { script: root.show() }
        SpringAnimation { target: root; property: "y"; spring: 2; damping: 0.2
            to: screen.desktopAvailableHeight - (height + 20)
        }
    }

    SequentialAnimation {
        id: hideAnimation
        SpringAnimation { target: root; property: "y"; spring: 2; damping: 0.2;
            to: screen.desktopAvailableHeight + (height + 20)
        }
        ScriptAction { script: root.hide() }
    }

    Timer {
        running: visible
        repeat: true
        triggeredOnStart: true
        interval: 1000
        onTriggered: {
            progress.value += 1
            var secondsLeft = progress.to - progress.value
            if (secondsLeft < 0) {
                stop()
                return
            }
            var mins = Math.floor(secondsLeft / 60)
            var secs = secondsLeft - mins * 60
            progress.text = (mins < 10 ? "0" + mins : mins) + ":" +
                (secs < 10 ? "0" + secs : secs)
        }
    }

    ColumnLayout {

        id: layout
        anchors.centerIn: parent

        Label { text: qsTr("Long break starts at: "); Layout.alignment: Qt.AlignCenter }

        CircleProgressBar {
            id: progress
            width: 150
            Layout.alignment: Qt.AlignCenter
        }

        Button {
            text: qsTr("Postpone")
            flat: true
            onClicked: postponeClicked()
            Layout.alignment: Qt.AlignCenter
        }
    }
}
