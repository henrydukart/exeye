#include "utils.h"
#include "activitymonitor.h"
#include "filelogger.h"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>
#include <QIcon>

void debugMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char *argv[])
{
    qInstallMessageHandler(debugMessageHandler);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);

    QApplication app(argc, argv);
    app.setOrganizationName("PopovDenis");
    app.setOrganizationDomain("popovdenis.com");
    app.setApplicationName("Exeye");
    app.setWindowIcon(QIcon(":/img/trayicon.png"));

    QTranslator translator;
    if (translator.load(QLocale(), QLatin1String("exeye"),
                        QLatin1String("_"), QLatin1String(":/trans")))
        app.installTranslator(&translator);

    qmlRegisterType<ActivityMonitor>("Qt.custom", 1, 0, "ActivityMonitor");
    qmlRegisterSingletonType<Utils>("Qt.custom", 1, 0, "Utils",
        [](QQmlEngine*, QJSEngine*) -> QObject * {
            return new Utils;
    });

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/src/qml/ConfigWindow.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

void debugMessageHandler(QtMsgType, const QMessageLogContext &, const QString &msg)
{
    static FileLogger logger;
    logger.log(msg);
}
