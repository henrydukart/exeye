#include "utils.h"

#include <QtMath>
#include <QFile>

Utils::Utils(QObject *parent) : QObject(parent)
{

}

bool Utils::isFileExists(const QString &filename)
{
    return QFile::exists(filename);
}

int Utils::hash(const QString &data)
{
    int hash = 0;
    for (int i = 0; i < data.length(); ++i)
        hash = (hash << 5) - hash + data[i].unicode();
    return hash;
}

QString Utils::milliSecsToString(uint msecs)
{
    uint totalSeconds = static_cast<uint>(qFloor(msecs / 1000));
    uint totalMinutes = static_cast<uint>(qFloor(totalSeconds / 60));

    uint hours = static_cast<uint>(qFloor(totalMinutes / 60));
    uint minutes = totalMinutes - hours * 60;
    uint seconds = totalSeconds - totalMinutes * 60;

    return QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char('0'))
                                .arg(minutes, 2, 10, QLatin1Char('0'))
                                .arg(seconds, 2, 10, QLatin1Char('0'));
}
