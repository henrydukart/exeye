#ifndef UTILS_H
#define UTILS_H

#include <QObject>

class Utils : public QObject
{
    Q_OBJECT
public:
    explicit Utils(QObject *parent = nullptr);

    Q_INVOKABLE static bool isFileExists(const QString& filename);
    Q_INVOKABLE static int hash(const QString& data);
    Q_INVOKABLE static QString milliSecsToString(uint msecs);

signals:

public slots:
};

#endif // UTILS_H
