#ifndef ACTIVITYMONITOR_H
#define ACTIVITYMONITOR_H

#include <QObject>

class QTimer;

class ActivityMonitor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool active READ active NOTIFY activeChanged)
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(unsigned timeout MEMBER m_timeout NOTIFY timeoutChanged)

public:
    explicit ActivityMonitor(QObject *parent = nullptr);

    bool active() const;

    void setEnabled(bool enabled);
    bool enabled() const;

private:
    void timerTriggered();

signals:
    void activeChanged(bool);
    void enabledChanged(bool);
    void timeoutChanged(unsigned);

private:
    bool m_isActive;
    bool m_isEnabled;
    unsigned m_timeout;

    QTimer* m_timer;
};

#endif // ACTIVITYMONITOR_H
