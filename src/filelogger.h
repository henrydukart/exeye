#ifndef FILELOGGER_H
#define FILELOGGER_H

#include <QFile>

class FileLogger
{
public:
    FileLogger();
    void log(const QString &msg);

private:
    QFile m_file;
};

#endif // FILELOGGER_H
