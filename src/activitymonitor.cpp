#include "activitymonitor.h"

#include <QTimer>

#include <windows.h>


namespace impl
{
    unsigned long GetLastActiveTime()
    {
        LASTINPUTINFO info;
        info.cbSize = sizeof(info);
        GetLastInputInfo(&info);
        return GetTickCount() - info.dwTime;
    }
}


ActivityMonitor::ActivityMonitor(QObject *parent)
    : QObject(parent)
    , m_isActive(true)
    , m_isEnabled(false)
    , m_timeout(60 * 1000)
    , m_timer(new QTimer(this))
{
    connect(m_timer, &QTimer::timeout, this, &ActivityMonitor::timerTriggered);
}

bool ActivityMonitor::active() const
{
    return m_isActive;
}

void ActivityMonitor::setEnabled(bool enabled)
{
    if (m_isEnabled == enabled)
        return;

    m_isEnabled = enabled;
    emit enabledChanged(enabled);

    if (!enabled)
    {
        m_timer->stop();
        return;
    }

    m_isActive = true;
    m_timer->setInterval(1000);
    m_timer->start();
}

bool ActivityMonitor::enabled() const
{
    return m_isEnabled;
}

void ActivityMonitor::timerTriggered()
{
    double diff = impl::GetLastActiveTime();

    if (m_isActive && diff >= m_timeout)
    {
        m_isActive = false;
        emit activeChanged(m_isActive);
    }
    else if (!m_isActive && diff < m_timeout)
    {
        m_isActive = true;
        emit activeChanged(m_isActive);
    }
}
